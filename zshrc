# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd beep nomatch notify
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/alistairf/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

source /usr/share/zsh/share/antigen.zsh
#source /usr/local/share/antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle npm
antigen bundle colored-man-pages
antigen bundle sudo
antigen bundle zsh_reload
antigen bundle z

# Theme Bundles
antigen bundle desyncr/auto-ls
antigen bundle mafredri/zsh-async
#antigen theme agnoster/agnoster-zsh-theme
#antigen theme justjanne/powerline-go
#antigen bundle dfurnes/purer
antigen bundle yourfin/pure-agnoster

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-completions

# Tell Antigen that you're done.
antigen apply

ZSH_AUTOSUGGEST_STRATEGY=(history)
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#aaaaaa"

autoload -Uz compinit
compinit
# Completion for kitty
#kitty + complete setup zsh | source /dev/stdin

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# use nvim, but don't make me think about it
alias vim="nvim"
